# microaudit

Firefox addon.
This is a merge of IndicateTLS (https://addons.mozilla.org/en-US/firefox/addon/indicatetls/) and IP Address and Domain Information (https://addons.mozilla.org/en-US/firefox/addon/ip-address-and-domain-info/) with some improvements.

## Asset

You can find XPI file into the web-ext-artifacts directory

## Example on duckduckgo web site

![microaudit example on duckduckgo web site](/images/microaudit_example.png)


## Features
- Check TLS with https://CryptCheck.fr, https://Observatory.Mozilla.org and https://SslLabs.com
- Check HTTP Headers with https://Securityheaders.com
- Get network informations (IPv4, IPv6, ISP, Domain) with https://DNSlytics.com