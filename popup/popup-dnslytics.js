const dnslyticResourcesDict = {
  "ipv4": "ipv4info",
  "ipv6": "ipv6info",
  "isp": "asinfo",
  "domain": "domaininfo"
}

async function updateDnslyticsTab(api) {
  browser.tabs.query({active: true, windowId: browser.windows.WINDOW_ID_CURRENT})
  .then(tabs => browser.tabs.get(tabs[0].id))
  .then(tab => {
    const hostname = (new URL(tab.url)).host;
    const elementDiv = document.getElementById('dnslytics-' + api);
    fetch('https://www.utlsapi.com/plugin.php?version=1.1&source=foxext'
                      + '&type=' + dnslyticResourcesDict[api]
                      + '&hostname=' + hostname)
    .then((resp) => resp.text())
    .then(function(html) {
      elementDiv.innerHTML = html
    })
    .catch(function(err) {  
        console.log('Failed to fetch page: ', err);  
    });
  }); 
}

/* Action listeners */
const buttonIPv4 = document.querySelector('#popup-button-ipv4');
buttonIPv4.addEventListener('click', () => {
  openTab(event, 'dnslytics-ipv4');
  updateDnslyticsTab('ipv4')
});

const buttonIPv6 = document.querySelector('#popup-button-ipv6');
buttonIPv6.addEventListener('click', () => {
  openTab(event, 'dnslytics-ipv6');
  updateDnslyticsTab('ipv6')
});

const buttonISP = document.querySelector('#popup-button-isp');
buttonISP.addEventListener('click', () => {
  openTab(event, 'dnslytics-isp');
  updateDnslyticsTab('isp')
});

const buttonDomain = document.querySelector('#popup-button-domain');
buttonDomain.addEventListener('click', () => {
  openTab(event, 'dnslytics-domain');
  updateDnslyticsTab('domain')
});
